{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9e8a59d4",
   "metadata": {},
   "source": [
    "# Pixel-based Particle Colocalization."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70e59a85-b9a5-4225-9c63-a4714066f07c",
   "metadata": {},
   "source": [
    "Let's look at some molecules through a microscope:\n",
    "\n",
    "<img style=\"display: block; max-width: 100%; height: auto; margin: auto; float: none!important;\" src=\"assets/correlated.png\">\n",
    "\n",
    "There are two kinds of particles, turquoise and pink, and they seem to be attracted to each other. They are close together. This happens quite often, by way of chemical interaction between the molecules. But not always:\n",
    "\n",
    "<img style=\"display: block; max-width: 100%; height: auto; margin: auto; float: none!important;\" src=\"assets/uncorrelated.png\">\n",
    "\n",
    "In this picture, it looks like the two kinds don't care for each other at all - they aren't particularly close to each other; uncorrelated. We might conclude there isn't any chemical interaction between them.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fabe54ed-8ec8-4617-a40e-f867941b966d",
   "metadata": {},
   "source": [
    "We want to quantify this notion of a correlation between two kinds of particles to have a statistical test - like <a href=\"https://en.wikipedia.org/wiki/Pearson%27s_chi-squared_test\"> Pearson's $\\chi^2$-test </a> - but taking in these microscopic images. The thing is that, at small scales, microscopes tend to produce blurry images:\n",
    "\n",
    "<img style=\"display: block; max-width: 100%; height: auto; margin: auto; float: none!important;\" src=\"assets/blurry.png\">\n",
    "\n",
    "Let's first try to work with these blurry images. We get an image (width $w$ and height $h$ in pixels) with two different channels representing the signal strength from both molecule types. For this, we need a number representing the correlation. So our correlation is a function\n",
    "\n",
    "$$f : \\mathbb R^{w \\times h \\times 2} \\to [-1, 1] $$\n",
    "\n",
    "where a correlation of 0 means a completely random distribution, 1 means exact alignment and -1 exact avoidance. Now we can treat each pixel as an observation of two random variables, the strength of the signal from each molecule, and run standard statistical correlation tests like the one mentioned above.\n",
    "\n",
    "\n",
    "## A Simulation\n",
    "Let's simulate the situation. We generate a pink point cloud of $n$ molecules by putting it randomly in the unit square. We then embed them into a bitmap by placing a gaussian distribution with standard deviation $b$ at the nearest pixel of the point. The $b$ stands for the blurriness of the microscope. \n",
    "\n",
    "Then, we can add another normal distribution to the pink point cloud to get the blue point cloud. This second Gaussian is centered and has standard deviation $\\sigma$. The larger $\\sigma$, the more 'uncorrelated' the two point clouds.\n",
    "\n",
    "We can also draw a line between corresponding blue and pink molecules, to see how far off they landed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bee1a998",
   "metadata": {},
   "outputs": [],
   "source": [
    "import piplite\n",
    "await piplite.install('ipywidgets')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8adcaca",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.colors as colors\n",
    "import matplotlib.pyplot as plt\n",
    "from ipywidgets import interact, interactive, fixed, interact_manual, FloatSlider, BoundedIntText, IntSlider, Checkbox, widgets\n",
    "from IPython.display import display\n",
    "from scipy.ndimage import gaussian_filter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "68f75deb-b488-4f9c-b6ee-779e3be060c1",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def show_correlation(n, blur_dist, corr, show_connection = False, seed=0, plot=True):\n",
    "    \"\"\"\n",
    "    Input:\n",
    "        n - an integer, the number of blue and pink dots each\n",
    "        blur_dist - a positive float, how much the bitmaps are blurred\n",
    "        corr - a positive float, the standard deviation of the offset between blue and pink points\n",
    "        show_connection - a boolean, whether or not to show connections between corresponding pink and blue dots\n",
    "    \n",
    "    Output:\n",
    "        A float, the computed correlation\n",
    "    \"\"\"\n",
    "    \n",
    "    # Resolution of bitmap\n",
    "    res = 256\n",
    "    \n",
    "    # Setting a seed for reproducability between \n",
    "    # different paramters\n",
    "    np.random.seed(seed)\n",
    "    \n",
    "    # Generating point clouds\n",
    "    p1 = np.random.random((n,2))\n",
    "    \n",
    "    # We simulate an actual correlation of the second particle\n",
    "    # type by adding a Gaussian to the first.\n",
    "    \n",
    "    p2 = p1 + np.random.normal(0, corr, (n,2))\n",
    "    \n",
    "    # Since we only consider a finite area, we wrap around\n",
    "    # particles off the square\n",
    "    \n",
    "    for p in p2:\n",
    "        p[0] = p[0] % 1\n",
    "        p[1] = p[1] % 1\n",
    "    \n",
    "    # Conversion into bitmaps\n",
    "    bm1 = np.zeros((res,res))\n",
    "    bm2 = np.zeros((res,res))\n",
    "\n",
    "    bm1[(p1 * res).astype(int)[:,0], (p1 * res).astype(int)[:,1]] = 1\n",
    "    bm2[(p2 * res).astype(int)[:,0], (p2 * res).astype(int)[:,1]] = 1\n",
    "\n",
    "    # Blurring the bitmaps\n",
    "    bm1_blur = gaussian_filter(bm1, blur_dist * res, mode = 'wrap')\n",
    "    bm2_blur = gaussian_filter(bm2, blur_dist * res, mode = 'wrap')\n",
    "    \n",
    "    \n",
    "    # The calculated correlation coefficient\n",
    "    ccc = np.corrcoef(np.vstack((bm1_blur.ravel(), bm2_blur.ravel())))[1,0]\n",
    "    \n",
    "    # Plotting (in red + green channels)\n",
    "    bm1_blur = np.maximum(1 - 700 * blur_dist * (bm1_blur / np.linalg.norm(bm1_blur)), 0)\n",
    "    bm2_blur = np.maximum(1 - 700 * blur_dist * (bm2_blur / np.linalg.norm(bm2_blur)), 0)\n",
    "    \n",
    "    mixed_image = np.stack((bm1_blur, bm2_blur, np.ones((res,res))), 2)\n",
    "    \n",
    "    if plot:\n",
    "        fig = plt.figure(figsize=(20,20))\n",
    "\n",
    "        plt.title(\"Computed Correlation Coefficient: {}\".format(np.round(ccc, 4)), fontsize=20)\n",
    "        plt.xticks([0, (res-1)/2 ,res-1], [\"0\", \"0.5\", \"1\"], fontsize=20)\n",
    "        plt.yticks([0, (res-1)/2, res-1], [\"0\", \"0.5\", \"1\"], fontsize=20)\n",
    "        plt.axis('off')\n",
    "\n",
    "        if show_connection:\n",
    "            # Show lines between the two associated points\n",
    "\n",
    "            p1 *= res\n",
    "            p2 *= res\n",
    "\n",
    "            for i in range(n):\n",
    "                plt.plot([p1[i][1], p2[i][1]], [p1[i][0], p2[i][0]], color='cornflowerblue', marker='')\n",
    "\n",
    "        plt.imshow(mixed_image / np.max(mixed_image))\n",
    "    return ccc\n",
    "\n",
    "\n",
    "# Creating widgets for interactive Visualization\n",
    "n_widget = IntSlider(description='$n$', \n",
    "                     min=2, max=20, value=8,\n",
    "                     continuous_update=False)\n",
    "blur_widget = FloatSlider(description='$b$', \n",
    "                          min=0.005, max=0.16, \n",
    "                          step=0.01, value=0.05,\n",
    "                          continuous_update=True)\n",
    "corr_widget = FloatSlider(description='$\\sigma$', \n",
    "                          min=0, max=0.3, \n",
    "                          step=0.01, value=0.1,\n",
    "                          continuous_update=False)\n",
    "connection_widget = Checkbox(description=\"Show Connections\",\n",
    "                             value = False)\n",
    "seed_widget = BoundedIntText(description='Seed', \n",
    "                             value=1,min=0)\n",
    "\n",
    "ui = widgets.VBox([n_widget, blur_widget, corr_widget, connection_widget, seed_widget])\n",
    "widget = interactive(show_correlation, n=10, blur_dist=0.04, corr=0.1, seed=0)\n",
    "out = widgets.interactive_output(show_correlation, {'n': n_widget, 'blur_dist': blur_widget, 'corr':corr_widget, 'show_connection':connection_widget, 'seed':seed_widget})\n",
    "\n",
    "display(ui, out)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6dae3c8a-2f49-41ff-a6d0-2a4c6608f676",
   "metadata": {},
   "source": [
    "## The Problem with Pointwise Correlation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9654117-6c48-4dee-b27b-9a1cc3c6b1fe",
   "metadata": {},
   "source": [
    "Notice anything in particular? There are a lot of things to discuss here, but how about this: The correlation coefficient is very much dependent on the blur, which can be fatal in practice. Let us further investigate this by plotting the correlation as a function of the blurriness. Again, $n$ is the number of particles for each type and $\\sigma$ is the standard deviation for the distance between the two types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e262e4a-e0e7-4b82-9109-607612694d65",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Creating widgets for interactive Visualization\n",
    "n_widget = IntSlider(description='$n$', \n",
    "                     min=2, max=20, value=8,\n",
    "                     continuous_update=False)\n",
    "corr_widget = FloatSlider(description='$\\sigma$', \n",
    "                          min=0, max=0.3, \n",
    "                          step=0.01, value=0.1,\n",
    "                          continuous_update=False)\n",
    "\n",
    "ui = widgets.VBox([n_widget, corr_widget])\n",
    "\n",
    "\n",
    "def plot_correlation(n, corr):\n",
    "    fig, ax = plt.subplots()\n",
    "    CCCs = []\n",
    "    blurs = np.linspace(0, 0.1, num=20)\n",
    "\n",
    "    for blur in blurs:\n",
    "        CCCs.append(show_correlation(n, blur, corr, plot=False))\n",
    "\n",
    "    ax.plot(blurs, CCCs, color=colors.hsv_to_rgb([0.03*n, 0.05*n, 0.5 + 0.02*n]))\n",
    "    \n",
    "    plt.xlabel('Blur Coefficient $b$')\n",
    "    plt.ylabel('Calculated Correlation Coefficient')\n",
    "    plt.title(\"Calculated Correlation for different Blurrinesses $b$ and number of Molecules $n$\")\n",
    "    plt.show()\n",
    "\n",
    "widget = interactive(plot_correlation, n=10, corr=0.1)\n",
    "out = widgets.interactive_output(plot_correlation, {'n': n_widget, 'corr':corr_widget})\n",
    "\n",
    "display(ui, out)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "924f2fd3-f73b-49f9-901d-fe75f2bad3c8",
   "metadata": {},
   "source": [
    "And once more for many different numbers of points at once:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec9e986c-1c48-4691-b03a-9e186d49b038",
   "metadata": {},
   "outputs": [],
   "source": [
    "ui = widgets.VBox([corr_widget])\n",
    "\n",
    "\n",
    "def plot_correlation_2(corr):\n",
    "    fig, ax = plt.subplots()\n",
    "    for n in range(1, 20):\n",
    "        CCCs = []\n",
    "        blurs = np.linspace(0, 0.1, num=20)\n",
    "\n",
    "        for blur in blurs:\n",
    "            CCCs.append(show_correlation(n, blur, corr, plot=False))\n",
    "\n",
    "        \n",
    "        ax.plot(blurs, CCCs, color=colors.hsv_to_rgb([0.03*n, 0.05*n, 0.5 + 0.02*n]), label='$n$ = '+str(n))\n",
    "    \n",
    "    ax.legend(bbox_to_anchor=(1, 1),\n",
    "          bbox_transform=fig.transFigure)\n",
    "    plt.xlabel('Blur Coefficient $b$')\n",
    "    plt.ylabel('Calculated Correlation Coefficient')\n",
    "    plt.show()\n",
    "\n",
    "widget = interactive(plot_correlation, n=10, corr=0.1)\n",
    "out = widgets.interactive_output(plot_correlation_2, {'corr':corr_widget})\n",
    "\n",
    "display(ui, out)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11937ecb-0ba8-4c01-b3ad-bf9f4b9bd7ec",
   "metadata": {},
   "source": [
    "And it looks like especially for low blurs, this pixel-wise method is not useful at all. No matter what value $\\sigma$ takes, the Correlation Coefficient is near 0. This is especially frustrating, because for good microscopes, i. e. low blur, we should get more accurate information about the particles' correlation, too.\n",
    "\n",
    "So, is there another way to look at the problem?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1698bce-ee22-423b-901b-4bde9154a1bd",
   "metadata": {},
   "source": [
    "## Optimal Transport to the Rescue"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "327128f8-271a-4fcd-8306-cde4a97f50cb",
   "metadata": {},
   "source": [
    "If [microscopes become really good](https://www.uni-goettingen.de/de/891.html?id=6699), we can move away from this pixel-based approach and look at the molecules as points in continuous space. The question then becomes: When are two _point clouds_ close together?\n",
    "\n",
    "This is where at we will consider in the [next notebook in this series](Colocalization_with_Optimal_Transport.ipynb), using optimal transport theory!\n",
    "\n",
    "****\n",
    "\n",
    "_Written by Thilo Stier and Lennart Finke. Published under the [MIT](https://mit-license.org/) license._"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.2 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  },
  "vscode": {
   "interpreter": {
    "hash": "a0a5145e6c304e2a9afaf5b930a2955b950bd4b81fe94f7c42930f43f42762eb"
   }
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
