# JupyterLite Demo Minimal Example

https://crc1456.pages.gwdg.de/livedocs/jupyterlite-optimal-transport

This Repo contains Notebooks from https://gitlab.gwdg.de/crc1456/livedocs/guided-jupyter-tour-optimal-transport
with small adjustements to make them run in JupyterLite.

# Notes:

 Adjustments that had to be made to run Jupyter notebooks in JupyterLite:


## Handeling Special Characters
1. Because of the build error that occured:
`Task 'build:contents:copy:data/2020-12-08_full/prep_2020-12-28_IQR-filter/linEmbHK_s=3.dat': name must not use the char '=' (equal sign).``

**CHANGES** 
`linEmbHK_s=3.dat` had to be renamed to `linEmbHK_s3.dat`
Acordingly, in `Optimal_Transport_and_X-ray_Tomography.ipynb`
Cell 2 line 21 hat to be adjusted to `    with open(filenameRoot+tagId+"/linEmbHK_s{}.dat".format(scale), 'rb') as f:
`

## `lib`folder not acessible
2. lib did not show up, so it had to be renamed from lib to libX.
Acording changes also in ``Optimal_Transport_and_X-ray_Tomography.ipynb``


## Installation of ipywidgets has to be done with piplite

```
import sys
if "pyodide" in sys.modules:
    import piplite
    await piplite.install('ipywidgets')
```